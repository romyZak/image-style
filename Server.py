from flask import Flask, render_template, request,send_file
from flask_uploads import UploadSet, configure_uploads, IMAGES
from stylize_image import Server as ServerMethod
import os
app = Flask(__name__)

photos = UploadSet('photos', IMAGES)

app.config['UPLOADED_PHOTOS_DEST'] = 'input'
configure_uploads(app, photos)

@app.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST' and 'photo' in request.files and 'style' in request.form:
        filename = 'input/'+photos.save(request.files['photo'])
        style= 'pretrained-networks/'+request.form['style']
        print(filename);
        print(style);
        ServerMethod(filename,style,'output/output.jpg');
        if os.path.exists(filename):
          os.remove(filename)
          print("Done");
        else:
          print("Sorry, I can not remove %s file." % filename)
        return send_file('output/output.jpg', mimetype='image/jpg')

    return render_template('View.html')


if __name__ == '__main__':
    app.run(debug=True)
